'use strict';

angular.module('angularFullstackApp')
  .controller('MainCtrl', function ($scope, $http, $sce) {

    $scope.escapeRegExp = function (string) {
      return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$1');
    };

    $scope.names = [];
    $scope.data = null;
    $scope.search = '';
    var controller = this;
    controller = this;
    controller.config = {
      autoPlay: true,
      sources: [
        {src: $sce.trustAsResourceUrl('http://static.videogular.com/assets/audios/videogular.mp3'), type: 'audio/mpeg'}
      ],
      theme:   {
        url: 'http://www.videogular.com/styles/themes/default/latest/videogular.css'
      }
    };
    controller.API = null;

    var regex;

    $http.get('/api/names').
      success(function (data, status, headers, config) {
        if(data !== null) {
          $scope.data = data;
          data.names.forEach(function (entry) {
            $scope.names.push(entry.name);
          });
          var f = status;
          f = headers;
          f = config;
        }
      }).
      error(function (data, status, headers, config) {
        console.log('Error[data]: ' + data);
        console.log('Error[status]: ' + status);
        console.log('Error[headers]: ' + headers);
        console.log('Error[config]: ' + config);
      });

    $scope.$watch('search', function (value) {
      regex = new RegExp('\\b' + $scope.escapeRegExp(value), 'i');
    });

    $scope.filterBySearch = function (name) {
      if (!$scope.search) {
        return true;
      }
      return regex.test(name);
    };

    $scope.playName = function (name) {
      var sources = null;

      $scope.data.names.forEach(function (entry) {
        if (entry.name === name) {
          sources = entry.sources;

          if (typeof sources !== 'undefined' && sources !== null) {
            if(controller.API.currentState === 'play') {
              controller.API.stop();
            }
            controller.config.sources = sources;
            controller.API.changeSource(sources);
            controller.API.play();
          }
          return false;
        }
      });


    };

    controller.onPlayerReady = function (API) {
      $scope.controller.API = API;
    };

  });
