/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /things              ->  index
 * POST    /things              ->  create
 * GET     /things/:id          ->  show
 * PUT     /things/:id          ->  update
 * DELETE  /things/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');

// Get list of things
exports.index = function(req, res) {
  res.json({
    "names": [
      {
        "name": "Aaliyah (ah-LEE-ah)",
        "sources": [ { "src": "/static/music/mayaclip.mp3", "type": "audio/mpeg" } ]
      },
      {
        "name": "Aaron (AIR-en)",
        "sources": [ { "src": "/static/music/mayaclip.mp3", "type": "audio/mpeg" } ]
      },
      {
        "name": "Abbey (AB-ee)",
        "sources": [ { "src": "/static/music/mayaclip.mp3", "type": "audio/mpeg" } ]
      },
      {
        "name": "Abbi (AB-ee)",
        "sources": [ { "src": "/static/music/mayaclip.mp3", "type": "audio/mpeg" } ]
      },
      {
        "name": "Abbie (AB-ee)",
        "sources": [ { "src": "/static/music/mayaclip.mp3", "type": "audio/mpeg" } ]
      },
      {
        "name": "Abbigail (AB-ih-gayl)",
        "sources": [ { "src": "/static/music/mayaclip.mp3", "type": "audio/mpeg" } ]
      },
      {
        "name": "Abby (AB-ee)",
        "sources": [ { "src": "/static/music/kellyclip.mp3", "type": "audio/mpeg" } ]
      },
      {
        "name": "Abe",
        "sources": [ { "src": "/static/music/kellyclip.mp3", "type": "audio/mpeg" } ]
      },
      {
        "name": "Zyon (ZYE-on)",
        "sources": [ { "src": "/static/music/mayaclip.mp3", "type": "audio/mpeg" } ]
      }
    ]
  });
};
