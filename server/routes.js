/**
 * Main application routes
 */

'use strict';

var errors = require('./components/errors');

module.exports = function(app) {

  // Insert routes below
  app.use('/api/things', require('./api/thing'));
  app.use('/api/names', require('./api/name'));

  var express = require('express');
  var serveStatic = require('serve-static');
  //app.use(express.static('./static'));
  app.use(express.static(process.env.PWD + '/htdocs'));

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
   .get(errors[404]);

  app.route('/static/*')
    .get(function(req, res) {
      res.sendfile(process.env.PWD + '/server/public/' + req.params[0]);
    });

  // All other routes should redirect to the index.html
  app.route('/*')
    .get(function(req, res) {
      res.sendfile(app.get('appPath') + '/index.html');
    });
};
